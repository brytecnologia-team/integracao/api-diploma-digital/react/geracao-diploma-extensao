import React, { useEffect, useState } from "react";
import Select from "react-select";
import api from "../../services/api";

import Extension from '../../components/Extension'

export default function Assinador() {

  // ESTADOS ONDE AS VARIÁVIES E VALORES DOS INPUTS FICAM ARMAZENADAS
  const [certificadosInstalados, setCertificadosInstalados] = useState("");
  const [certificadoSelecionado, setCertificadoSelecionado] = useState("");
  const [tipoAssinante, setTipoAssinante] = useState("Representantes");
  const [tipoDocumento, setTipoDocumento] = useState("XMLDiplomado")
  const [assinando, setAssinando] = useState(false)
  const [documento, setDocumento] = useState(null);

  // ESTADOS ONDE AS VARIÁVEIS DE COPIA FICAM ARMAZENADAS
  const [docAcademica, setDocumentoAcademica] = useState(null);
  const [docDiplomado, setDocumentoDiplomado] = useState(null);
  const [loadingCopia, setLoadingCopia] = useState(false);

  // VERIFICA SE A EXTENSÃO DA BRY ESTÁ INSTALADA
    const extensaoInstalada = window.BryWebExtension.isExtensionInstalled();

  // USANDO A EXTENSÃO, VERIFICA OS CERTIFICADOS INSTALADOS E ARMAZENA NO ESTADO DA APLICAÇÃO
  useEffect(() => {
    if (extensaoInstalada) {
        window.BryWebExtension.listCertificates().then(certificados => {
        certificados.forEach(certificado => {
          certificado.label = certificado.name;
        });
        setCertificadosInstalados(certificados);
      });
    }
  }, [extensaoInstalada]);

  // FUNÇÃO QUE SERÁ EXECUTADA AO ENVIAR O FORMULÁRIO
  function enviarFormulario(event) {
    event.preventDefault();
    setAssinando(true);
    assinar();
  }

  // FUNÇÃO QUE SERÁ CHAMADA PELO FORMULÁRIO PARA ASSINAR O DOCUMENTO
  async function assinar() {

    // CRIA UM FORMDATA QUE SERÁ ENVIADO PARA O BACKEND
    const dados = new FormData();
    dados.append('documento', documento);
    dados.append('tipoAssinante', tipoAssinante);
    dados.append('certificate', certificadoSelecionado.certificateData);

    // FAZ A REQUISIÇÃO DE INICIALIZAÇÃO DE ASSINATURA PARA O BACKEND
    try {
      const resposta = await api.post(`/${tipoDocumento}/inicializa`, dados);
      console.log("response", resposta);

      if(resposta.data.message) {
        // SE TEM ALGUMA MENSAGEM DE ERRO, MOSTRA A MENSAGEM
        alert(resposta.data.message)
      } else {    
        // SE A INICIALIZAÇÃO OCORREU DE MANEIRA CORRETA, CHAMA A FUNÇÃO QUE IRÁ CIFRAR OS DADOS USANDO A EXTENSÃO
        assinarExtensao(resposta.data, certificadoSelecionado.certificateData);
      }
    } catch (err) {
      console.log(err.resposta);
      alert(err.resposta);
    }
    setAssinando(false);
  }
  async function assinarExtensao(respostaInicializacao, certificado) {

    console.log(respostaInicializacao);

    const inputExtension = {};

    // CONFIGURA O O OBJETO QUE RETORNOU DA INICIALIZAÇÃO DA MANEIRA QUE A EXTENSÃO RECEBE
    inputExtension.formatoDadosEntrada = 'Base64'
    inputExtension.formatoDadosSaida = 'Base64'
    inputExtension.algoritmoHash = 'SHA256'
    inputExtension.assinaturas = [{}];
    inputExtension.assinaturas[0].hashes = respostaInicializacao.signedAttributes[0].content;
    inputExtension.assinaturas[0].nonce = respostaInicializacao.signedAttributes[0].nonce;
    
    inputExtension.assinaturas.forEach(
      assinatura => (assinatura.hashes = [assinatura.hashes])
    );

    console.log(inputExtension);

    // USA A EXTENSÃO PARA CIFRAR OS DADOS
      await window.BryWebExtension.sign(
      certificadoSelecionado.certId,
      JSON.stringify(inputExtension)
    ).then(async assinatura => {

      console.log("assinatura")
      console.log(assinatura)

      // SE OS DADOS FORAM CIFRADOS, CRIA UM FORMDATA QUE SERÁ ENVIADO PARA A FINALIZAÇÃO
      const dados = new FormData();
      dados.append('initializedDocuments', respostaInicializacao.initializedDocuments[0].content);
      dados.append('certificate', certificado);
      dados.append('cifrado', assinatura.assinaturas[0].hashes[0]);
      dados.append('tipoAssinante', tipoAssinante);
      dados.append('documento', documento);

      // FAZ A REQUISIÇÃO DE FINALIZAÇÃO PARA O BACKEND
      try {
        const respostaRequisicao = await api.post(`/${tipoDocumento}/finaliza`, dados);
        
        setAssinando(false);
        console.log("response", respostaRequisicao);
        if(respostaRequisicao.data.message) {
          // SE TEM ALGUMA MENSAGEM DE ERRO, MOSTRA A MENSAGEM
          alert(respostaRequisicao.data.message)
        } else {
          // REALIZA O DOWNLOAD DO DIPLOMA ASSINADO
          window.location.href = respostaRequisicao.data.documentos[0].links[0].href;
        }
      } catch (err) {
        alert(err);
      }
    }).catch( err => {
      console.log(err);
    });
  }

    // FUNÇÃO QUE SERÁ EXECUTADA AO ENVIAR O FORMULÁRIO DE COPIA
    async function handleCopy(event) {
      // Evita que a página atualize ao dar um submit
      event.preventDefault();
  
      setLoadingCopia(true);
  
      const dados = new FormData();
      dados.append('documentacaoAcademica', docAcademica);
      dados.append('XMLDiplomado', docDiplomado);
  
      try {
        // REALIZA REQUISIÇÃO PARA O BACKEND
        const response = await api.post(`http://localhost:3333/XMLDiplomado/copia-nodo`, dados);
        console.log("response", response);
        if(response.data.message) {
          alert(response.data.message)
        } else {
          // REALIZA O DOWNLOAD DO DIPLOMA COM A COPIA
          const element = document.createElement("a");
          const file = new Blob([response.data], { type:"text/xml"});
          element.href = URL.createObjectURL(file);
          element.download = "diplomado-com-copia-de-nodo.xml";
          document.body.appendChild(element);
          element.click();
          alert("Ok");
        }
  
      } catch (err) {
        // DA UM ALERT COM A MENSAGEM DE ERRO
        alert(err);
      }
  
      setLoadingCopia(false);
    }

  // HTML DO FRONTEND
  return (
    <>
      {extensaoInstalada ? (
        <div style={{display:"flex"}}>
          <form onSubmit={enviarFormulario}>
            <h2>Assinador de Diploma Digital</h2>
            <label htmlFor="certificate">Selecione o certificado que deseja utilizar para assinar*</label>
            <Select
              id="certificate"
              options={certificadosInstalados}
              onChange={event => setCertificadoSelecionado(event)}
              value={certificadoSelecionado}
              required
            />

            {certificadoSelecionado ? (
              <React.Fragment>
                <input
                  type="text"
                  readOnly
                  value={certificadoSelecionado.issuer || ""}
                />
                <input
                  type="text"
                  readOnly
                  value={certificadoSelecionado.expirationDate || ""}
                />
                <input
                  type="text"
                  readOnly
                  value={certificadoSelecionado.certificateType || ""}
                />
                <textarea
                  type="text"
                  rows="5"
                  value={certificadoSelecionado.certificateData}
                  readOnly
                />
              </React.Fragment>
            ) : (
              ""
            )}
            
            <label htmlFor="docLabel">Documento a ser assinado</label>
              <label htmlFor="documento" className="fileUp">
                {documento ? (
                  <React.Fragment>{documento.name}</React.Fragment>
                ) : (
                    <React.Fragment>
                      <i className="fa fa-upload"></i>
                    Selecione o arquivo
                    </React.Fragment>
                  )}
                <input
                  id="documento"
                  type="file"
                  onChange={event => setDocumento(event.target.files[0])}
                />
              </label>
            
            <label htmlFor="tipoDocumento">Tipo de Documento*</label>
            <select
              name="tipoDocumento"
              value={tipoDocumento}
              onChange={event => {setTipoDocumento(event.target.value) ; setTipoAssinante("Representantes") }}
              required
            >
              <option value="XMLDiplomado">XML Diplomado</option>
              <option value="XMLDocumentacaoAcademica">XML Documentação Acadêmica</option>
            </select>

            <label htmlFor="tipoAssinante">Tipo de Assinante*</label>
            <select
              name="tipoAssinante"
              value={tipoAssinante}
              onChange={event => setTipoAssinante(event.target.value)}
              required
            >
                <option value="Representantes">Representantes</option>
                {tipoDocumento === "XMLDiplomado" ? 
                  (
                    <>
                      <option value="IESRegistradora">Registradora da IES</option>            
                    </> 
                  ) :
                  (
                    <>
                      <option value="IESEmissoraDadosDiploma">IES Emissora em Dados Diploma</option>   
                      <option value="IESEmissoraRegistro">IES Emissora para registro</option>           
                    </> 
                  )
                }
            </select>

            <button className="btn" type="submit">
              Assinar
            </button>

            <label>
                {assinando ? "Realizando a assinatura do documento..." : ""}
            </label>
          </form>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
          <form onSubmit={handleCopy}>
            <h2>Exemplo de copia do nodo Dados Diploma</h2>
            <link
              rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
            />

            <label htmlFor="docLabel">Documentação Acadêmica</label>
            <label htmlFor="docAcademica" className="fileUp">
              {docAcademica ? (
                <React.Fragment>{docAcademica.name}</React.Fragment>
              ) : (
                  <React.Fragment>
                    <i className="fa fa-upload"></i>
                  Selecione o arquivo
                  </React.Fragment>
                )}
              <input
                id="docAcademica"
                type="file"
                onChange={event => setDocumentoAcademica(event.target.files[0])}
              />
            </label>

            <label htmlFor="docLabel">Documentação Diplomado</label>
            <label htmlFor="docDiplomado" className="fileUp">
              {docDiplomado ? (
                <React.Fragment>{docDiplomado.name}</React.Fragment>
              ) : (
                  <React.Fragment>
                    <i className="fa fa-upload"></i>
                  Selecione o arquivo
                  </React.Fragment>
                )}
              <input
                id="docDiplomado"
                type="file"
                onChange={event => setDocumentoDiplomado(event.target.files[0])}
              />
            </label>

            <button className="btn" type="submit">
              Copiar
            </button>

            <div>{loadingCopia ? "Realizando a copia do documento..." : ""}</div>
          </form>
        </div>
      // SE A EXTENSÃO NÃO ESTIVAR INSTALADA, RENDERIZA O COMPONENTE DE MANUAL DE INSTALAÇÃO
      ) : (<Extension/>)}
    </>
  );
}
