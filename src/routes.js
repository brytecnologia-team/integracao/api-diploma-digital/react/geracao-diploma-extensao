import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Assinador from './pages/Assinador';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Assinador} />
      </Switch>
    </BrowserRouter>
  );
}
